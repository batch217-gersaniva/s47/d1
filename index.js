// console.log("Hello World");

//txtFirstName--->variable
const txtFirstName = document.querySelector("#txt-first-name");  
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// call updateFullName to write down what is inputted in firstname and lastname using arrow function
const updateFullName = () => {

// firstName/lastName -> declaration
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;


// inner <span id="span-full-name">________</span>
	spanFullName.innerHTML = `${firstName} ${lastName}`;


}
// inside () are arguments, "keyup" is an event
txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);